Rails.application.routes.draw do
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html

  root 'witch_sagas#index'

  resources :witch_sagas, only: :index do
    collection do 
      get :results
    end
  end
end
