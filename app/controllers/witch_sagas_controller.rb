class WitchSagasController < ApplicationController
  before_action :get_average_number, only: :results

  def index;end

  def results;end

  private

    def get_average_number
      @first_person_born = params[:first_year_of_death].to_i - params[:first_age_of_death].to_i
      @second_person_born = params[:second_year_of_death].to_i - params[:second_age_of_death].to_i

      @first_number_people_killed = WitchSaga.number_killed_on_year(@first_person_born)
      @second_number_people_killed = WitchSaga.number_killed_on_year(@second_person_born)

      @average = (@second_number_people_killed.to_f + @first_number_people_killed.to_f) / 2.0
    end
end