class WitchSaga
  class << self
    def number_killed_on_year(year)
      array_number = []
      (1..year.to_i).each do |i|
        if array_number.count < 1
          array_number << i
        else
          next_number = array_number.last(2).sum
          array_number << next_number
        end
      end

      return array_number.sum()
    end
  end
end